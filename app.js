const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const mongoose = require('mongoose')
require('dotenv').config() // เรียกใช้ dotevn หลังจากลงเสร็จ

var mongooseconnect = mongoose.connection
var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')

var app = express()
// เชื่อมต่อ mongodb
// var { DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS } = process.env
// mongoose.connect(`mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`, {
//   user: `${DB_USER}`,
//   pass: `${DB_PASS}`,
//   useNewUrlParser: true,
//   useCreateIndex: true,
//   authMechanism: 'SCRAM-SHA-1'
// })

mongoose.connect('mongodb://localhost:27017/localDB')
mongooseconnect.on('connected', function () {
  console.log('Alert Message \nMongoose is Connected')
})
mongooseconnect.on('error', function (err) {
  console.log('Alert Message \n' + err)
})

// console.log(process.env)

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/', usersRouter)
// app.use('/', studentRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

// app.listen(1234)
module.exports = app
