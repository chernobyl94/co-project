const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Student = new Schema({
  firstname: String,
  lastname: String,
  age: Number
})

module.exports = mongoose.model('Students', Student)
