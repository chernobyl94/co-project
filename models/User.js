const mongoose = require('mongoose')
const moment = require('moment-timezone')
const dateThailand = moment.tz(Date.now(), 'Asia/Bangkok')
const Schema = mongoose.Schema
const User = new Schema({
  firstname: String,
  lastname: String,
  age: Number,
  createdatetime: Date
})
module.exports = mongoose.model('Users', User)
